__source_test_file_main_str__ = """"""


def begining_of_source_test_file_str(deploy_unit_name, device_declaration_str):
    return f"""#include <iostream>
#include "Test{deploy_unit_name}.h"\n\n
int main(int argc, char **argv)
{{
    std::cout << "\\nRunning main() from gtest_main.cc" << std::endl;
    Test{deploy_unit_name}::server_name = std::string(argv[1]);
{device_declaration_str}
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();

}}

namespace Test{deploy_unit_name}
{{

// Make your changes below
"""

__ending_of_source_test_file_str__ = "// End of making changes\n\n} // end namespace\n"

def script_template_to_configure_and_run_tests_str(deploy_unit_name, fesa_class_test_file_path, # pylint: disable=R0913
                                                name_device, provide_device, attr_device, help_options, 
                                                            short_options, long_options, case_options):
    return f"""#!/bin/sh
# Script to configure and run google FESA Class tests
#============================================================================
#% SYNOPSIS
#+    ${{SCRIPT_NAME}} [-d*] [-s] [-i] [-r]
#%
#% DESCRIPTION
#%    This script is used to compile and run google FESA tests
#%
#% OPTIONS
{help_options}
#%    -s, --server_name             set the server name value
#%    -i, --instance_script_run     set the path to instance script runner
#%    -r, --ready_flag              set the flag on, server is running
#%    -h, --help, --man             print this help
#%
#% SOURCE
#%    https://git.gsi.de/t.madej/fesa-test-generator
#============================================================================
usage() {{ printf "Usage: "; head -50 ${{0}} | grep "^#+" | sed -e "s/^#+[ ]*//g" -e "s/\${{SCRIPT_NAME}}/${{SCRIPT_NAME}}/g" ; }}
usagefull() {{ head -50 ${{0}} | grep -e "^#[%+-]" | sed -e "s/^#[%+-]//g" -e "s/\${{SCRIPT_NAME}}/${{SCRIPT_NAME}}/g" ; }}

unset SCRIPT_NAME SCRIPT_OPTS ARRAY_OPTS

SCRIPT_NAME="$(basename ${0})"
OptFull=$@
OptNum=$#
BASEDIR=$(pwd)

SCRIPT_OPTS=':d{short_options}s:i:-:h-:r'
typeset -A ARRAY_OPTS
ARRAY_OPTS=(
{long_options}
    [server_name]=s
    [instance_script_run]=i
    [ready_flag]=r
    [help]=h
    [man]=h
)

while getopts ${{SCRIPT_OPTS}} OPTION ; do
    if [[ "x$OPTION" == "x-" ]]; then
        LONG_OPTION=$OPTARG
        LONG_OPTARG=$(echo $LONG_OPTION | grep "=" | cut -d'=' -f2)
        LONG_OPTIND=-1
        [[ "x$LONG_OPTARG" = "x" ]] && LONG_OPTIND=$OPTIND || LONG_OPTION=$(echo $OPTARG | cut -d'=' -f1)
        [[ $LONG_OPTIND -ne -1 ]] && eval LONG_OPTARG="\$$LONG_OPTIND"
        OPTION=${{ARRAY_OPTS[$LONG_OPTION]}}
        [[ "x$OPTION" = "x" ]] &&  OPTION="?" OPTARG="-$LONG_OPTION"
        
        if [[ $( echo "${{SCRIPT_OPTS}}" | grep -c "${{OPTION}}:" ) -eq 1 ]]; then
            if [[ "x${{LONG_OPTARG}}" = "x" ]] || [[ "${{LONG_OPTARG}}" = -* ]]; then 
                OPTION=":" OPTARG="-$LONG_OPTION"
            else
                OPTARG="$LONG_OPTARG";
                if [[ $LONG_OPTIND -ne -1 ]]; then
                    [[ $OPTIND -le $Optnum ]] && OPTIND=$(( $OPTIND+1 ))
                    shift $OPTIND
                    OPTIND=1
                fi
            fi
        fi
    fi

    if [[ "x${{OPTION}}" != "x:" ]] && [[ "x${{OPTION}}" != "x?" ]] && [[ "${{OPTARG}}" = -* ]]; then 
        OPTARG="$OPTION" OPTION=":"
    fi

    case "$OPTION" in
        r) flag_ready=true ;;
        d) ;;
{case_options}
        s) server_name=${{OPTARG}}         ;;
        i) run_instance_script=$(realpath -s "${{OPTARG}}") ;;
        h) usagefull && exit 0             ;;
        :) echo "${{SCRIPT_NAME}}: -$OPTARG: option requires an argument" >&2 && usage >&2 && exit 99 ;;
        ?) echo "${{SCRIPT_NAME}}: -$OPTARG: unknown option" >&2 && usage >&2 && exit 99 ;;
    esac
done
shift $((${{OPTIND}} - 1))
echo@new
#colors
RED='\033[0;31m'
BLUE='\033[0;34m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

################################################################################################################################
printf "\\n${{BLUE}}=============Compile Google-Tests Of FESA Class=============${{NC}}\\n"
make test
COMPILE_RESPONSE=$?
if [[ $COMPILE_RESPONSE != "0" ]]; then
    printf  "${{RED}}=============Compilation Failed (error code: $COMPILE_RESPONSE)=============${{NC}}"
    exit
fi
printf "\\n${{GREEN}}=============The compilation has just been successful=============${{NC}}\\n"
################################################################################################################################
if [ -z "$run_instance_script" ]; then
    if [ ! $flag_ready ]; then
        read -r -p "Please start your FESA class now. Done? [y/n]: " response
        if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
        then
            continue
        else
            printf "${{RED}}============FEC not running, script aborted!=============${{NC}}\\n"
            exit
        fi
    fi
else
    {name_device}
    server_name=$(xmllint --xpath "//information/fec-name/text()" "$(dirname "$run_instance_script")/"DeviceData_*.instance)
    if [[ ! -x $run_instance_script ]]; then
        printf "\\n${{RED}}=============The script in the given path does not exist or is not executable!=============${{NC}}\\n"
        exit
    fi
    cd $(dirname "$run_instance_script")
    $run_instance_script &
    PID_FESA_Instance_Running=$!
    cd $BASEDIR
    sleep 2
    if [[ "" ==  "$PID_FESA_Instance_Running" ]]; then
        printf "\\n${{RED}}========An error occurred while starting the fesa server========${{NC}}\\n"
        exit
    fi
fi
################################################################################################################################
{provide_device}
if [ -z "$server_name" ]; then
    read -r -p "Please provide server name: " server_name
fi
################################################################################################################################
printf "\\n${{BLUE}}=============Run Google-Tests Of FESA Class=============${{NC}}\\n"
{fesa_class_test_file_path}/test/bin/x86_64/gtest_{deploy_unit_name}-client $server_name{attr_device}
TEST_RESPONSE=$?
if [[ $TEST_RESPONSE == "0" ]]
then
    printf "\\n${{GREEN}}=============Google Tests have just Passed=============${{NC}}\\n"
else
    printf "\\n${{RED}}=============Google Tests have just Failed (error code: $TEST_RESPONSE)=============${{NC}}\\n"
fi
################################################################################################################################
if [ -n "$run_instance_script" ]; then
    if [[ "" !=  "$PID_FESA_Instance_Running" ]]; then
        printf "\\n${{BLUE}}========Killing PID $PID_FESA_Instance_Running Of Running FESA Class Instance========${{NC}}\\n"
        kill -9 $PID_FESA_Instance_Running
    fi
fi
################################################################################################################################
printf "\\n${{GREEN}}====================================DONE!======================================${{NC}}\\n"
################################################################################################################################
"""

def header_file_test_template_str(deploy_unit_name, device_definition_str):
    return f"""#ifndef SRC_TEST_{deploy_unit_name.upper()}_H_
#define SRC_TEST_{deploy_unit_name.upper()}_H

#include "include/IntegrationTestRDAClient.h"
#include "include/IntegrationTestListener.h"

// googletest include
#include <gtest/gtest.h>

// system includes
#include <string>
#include <iostream>

namespace Test{deploy_unit_name}
{{

{device_definition_str}
std::string server_name = "";
const std::string deploy_unit_name = "{deploy_unit_name}";

class Test{deploy_unit_name} : public ::testing::Test, public IntegrationTestRDAClient, public IntegrationTestListener
{{
public:
    void SetUpDevice(std::string device_name);
    static void SetUpTestCase();
}};

void Test{deploy_unit_name}::SetUpDevice(std::string device_name)
{{
    ASSERT_NO_THROW(
        auto_ptr<Data> data = DataFactory::createData();
        executeSet(device_name,"Init", data); //This will fill the fields of the class with data
    );
}} // SetUpDevice()

void Test{deploy_unit_name}::SetUpTestCase()
{{
    connectToRemoteServer(deploy_unit_name, server_name);
}} // SetUpTestCase()


}} // end namespace

#endif /* SRC_TEST_{deploy_unit_name.upper()}_H_ */
"""

def template_assert_test_version_str(fesa_version, class_version, deploy_unit_version):
    return f"""
//            ASSERT_FALSE(strcmp(value->getData().getString("fesaVersion"), "{fesa_version}"));
//            ASSERT_FALSE(strcmp(value->getData().getString("deployUnitVersion"), "{deploy_unit_version}"));
//            ASSERT_FALSE(strcmp(value->getData().getString("classVersion"), "{class_version}"));
"""

def template_assert_test_module_status_str():
    return """
// // This example only show how GSI-ModuleStatus-Property test can look like
//            size_t moduleStatus_size = 2;
//            const int* moduleStatus  = value->getData().getArrayInt("moduleStatus", moduleStatus_size);
//            ASSERT_EQ(moduleStatus[0], fill_with_expected_data);
//            ASSERT_EQ(moduleStatus[1], fill_with_expected_data);

//            const char * const *  moduleStatus_labels = value->getData().getArrayString("moduleStatus_labels", moduleStatus_size);
//            ASSERT_STREQ(moduleStatus_labels[0], fill_with_expected_data); 
//            ASSERT_STREQ(moduleStatus_labels[1], fill_with_expected_data);
"""

def template_assert_test_status_str():
    return """
// // This example only show how GSI-Status-Property test can look like
//            int32_t status = value->getData().getInt("status");
//            size_t detailedStatus_size = 2;
//            const bool* detailedStatus  = value->getData().getArrayBool("detailedStatus", detailedStatus_size);
//            const int* detailedStatus_severity  = value->getData().getArrayInt("detailedStatus_severity", detailedStatus_size);
//            const char* const* detailedStatus_labels = value->getData().getArrayString("detailedStatus_labels", detailedStatus_size);
//            int32_t powerState = value->getData().getInt("powerState");
//            int32_t control = value->getData().getInt("control");
//            bool interlock = value->getData().getBool("interlock");
//            bool opReady = value->getData().getBool("opReady");
//            bool modulesReady = value->getData().getBool("modulesReady");

//            ASSERT_EQ(status, fill_with_expected_data);
//            ASSERT_TRUE/FALSE(detailedStatus[0]);
//            ASSERT_TRUE/FALSE(detailedStatus[1]);
//            ASSERT_EQ(detailedStatus_severity[0], fill_with_expected_data);
//            ASSERT_EQ(detailedStatus_severity[1], fill_with_expected_data);
//            ASSERT_STREQ(detailedStatus_labels[0], fill_with_expected_data);
//            ASSERT_STREQ(detailedStatus_labels[1], fill_with_expected_data);
//            ASSERT_EQ(powerState, fill_with_expected_data);
//            ASSERT_EQ(control, fill_with_expected_data);
//            ASSERT_TRUE/FALSE(opReady);
//            ASSERT_TRUE/FALSE(modulesReady);
//            ASSERT_TRUE/FALSE(interlock);
"""

def template_test_get_value_item_str(deploy_unit_name, fesa_class_name, name_test,
                                        specific_assertions=""):
    return f"""
    TEST_F(Test{deploy_unit_name}, {fesa_class_name}_Get_{name_test})
    {{
        SetUpDevice({fesa_class_name}_device_name);
//// Fill up as needed!
        ASSERT_NO_THROW(
//            std::auto_ptr<AcquiredData> value = executeGet({fesa_class_name}_device_name, "{name_test}");
////            std::cout << value->getData().toString() << endl; // Check the result manually
{specific_assertions}
        );
    }}
"""

def template_test_set_and_get_value_item_str(deploy_unit_name, fesa_class_name, name_test,
                                                                            append_item_str):
    return f"""
    TEST_F(Test{deploy_unit_name}, {fesa_class_name}_Set_Get_{name_test})
    {{
        SetUpDevice({fesa_class_name}_device_name);
//// Fill up as needed!
        ASSERT_NO_THROW(
//            auto_ptr<Data> data = DataFactory::createData();
{append_item_str}
//            auto_ptr<Data> check_data = data->clone();
//            executeSet(device_name, "{name_test}", data);

//            std::auto_ptr<AcquiredData> value = executeGet({fesa_class_name}_device_name, "{name_test}");
//            ASSERT_TRUE(value->getData().equals(AcquiredData(check_data).getData()));
        );
    }}
"""

def intergration_test_listener_template_str():
    return """#ifndef INTEGRATION_TEST_LISTENER_H_
#define INTEGRATION_TEST_LISTENER_H_
#include <cmw-rda3/client/service/ClientServiceBuilder.h>
#include <cmw-rda3/common/config/Defs.h>
using namespace cmw::util;
using namespace cmw::data;
using namespace cmw::rda3::client;
using namespace cmw::rda3::common;
// The callback object that is used to handle subscription results

class IntegrationTestListener: public NotificationListener
{
    public:
        IntegrationTestListener()
        {
            dataAvailable_ = false;
            numberOfValidUpdates_ = 0;
            //auto_ptr<Data> data = DataFactory::createData();
            receivedData_.reset(new AcquiredData(DataFactory::createData()));
            //TODO: fill auto-ptr with empty data to avoid seg-faults
        }
        virtual void dataReceived(const Subscription & subscription, std::auto_ptr<AcquiredData> value, UpdateType updateType)
        {
            if( updateType == UT_NORMAL || updateType == UT_IMMEDIATE_UPDATE )
            {
                //To allow easy debugging:
                if (value.get() != NULL)
                {
                    numberOfValidUpdates_++;
                    dataAvailable_ = true;
                    receivedData_ = value;
                    //For debugging
                    //std::cout << receivedData_->getData().toString() << std::endl;
                }
                else
                {
                    std::string error = "IntegrationTestListener: No Data available for this update";
                    std::cout << error << std::endl;
                    throw error;
                }
            }
            else if(updateType == UT_FIRST_UPDATE) //we dont care for the first-update of subscriptions
            {
                std::cout << "IntegrationTestListener - UT_FIRST_UPDATE was ignored" << std::endl;
            }
        }
        void errorReceived(const Subscription & subscription, std::auto_ptr<RdaException> exception, UpdateType updateType)
        {
            if( updateType != UT_FIRST_UPDATE )
            {
                std::cout << "error received in SubscriptionReportHandler" << std::endl;
                std::cout << exception->what() << std::endl;
                std::cout << std::endl;
            }
        }
        std::auto_ptr<AcquiredData> receivedData_;
        bool dataAvailable_;
        unsigned int numberOfValidUpdates_;
};
typedef shared_ptr<IntegrationTestListener>::type TestListenerSharedPtr;
#endif /* INTEGRATION_TEST_LISTENER_H_ */
"""

def intergration_test_rda_client_template_str():
    return """#ifndef INTEGRATION_TEST_RDA_CLIENT_H_
#define INTEGRATION_TEST_RDA_CLIENT_H_

#include <cmw-rda3/client/service/ClientServiceBuilder.h>

using namespace std;
using namespace cmw::util;
using namespace cmw::data;
using namespace cmw::rda3::client;
using namespace cmw::rda3::common;

// system includes
#include <string>
#include <iostream>
#include <unistd.h>

class IntegrationTestRDAClient
{
private:


public:

    static std::string server_name_;
    static auto_ptr<ClientService> clientService_;
    static void setServerName(std::string serverName);
    static void connectToServer(std::string serverName);
    static void connectToLocalServer(std::string deploy_unit_name);
    static void connectToRemoteServer(std::string deploy_unit_name, std::string hostname);

    static void printException(const RdaException& ex);
    static void executeSet(std::string device_name, std::string property_name, auto_ptr<Data> data , std::string cycleName = "");
    static void executeFilteredSet( std::string device_name, std::string property_name, auto_ptr<Data> data, auto_ptr<Data> filter, std::string cycleName="" );
    static std::auto_ptr<AcquiredData> executeGet(std::string device_name, std::string property_name, std::string cycleName = "");
    static std::auto_ptr<AcquiredData> executeFilteredGet(std::string device_name, std::string property_name, auto_ptr<Data> filter, std::string cycleName = "");
    static SubscriptionSharedPtr subscribe(std::string device_name, std::string property_name, NotificationListenerSharedPtr replyHandler, std::string cycleName = "");
    static bool unsubscribe(SubscriptionSharedPtr& sub);

};

//init static members
std::string IntegrationTestRDAClient::server_name_ = "";
auto_ptr<ClientService> IntegrationTestRDAClient::clientService_ = (auto_ptr<ClientService>)NULL;

void IntegrationTestRDAClient::setServerName(std::string serverName)
{
    server_name_ = serverName;
    std::cout << "Used server_name: " << server_name_ << std::endl;
}

void IntegrationTestRDAClient::connectToServer(std::string serverName)
{
    server_name_ = serverName;
    clientService_ = ClientServiceBuilder::newInstance()->build();
    std::cout << "Used server_name: " << server_name_ << std::endl;
}

void IntegrationTestRDAClient::connectToLocalServer(std::string deploy_unit_name)
{
    char hostname[128];
    gethostname(hostname, sizeof hostname);
    // FIXME does not work properly because hostname() returns hostname including network domain
    connectToServer(deploy_unit_name + "." + hostname );
}

void IntegrationTestRDAClient::connectToRemoteServer(std::string deploy_unit_name, std::string hostname)
{
    connectToServer(deploy_unit_name + "." + hostname.c_str() );
}

void IntegrationTestRDAClient::printException(const RdaException& ex)
{
    std::cout << "Exception caught during Test:" << std::endl;
    cout << ex.what() << endl;
    std::vector<std::string> stack = ex.getStack();
    std::vector<std::string>::iterator iter;
    for ( iter = stack.begin(); iter != stack.end(); iter++ )
    {
        std::cout << *iter << std::endl;
    }
}

void IntegrationTestRDAClient::executeSet( std::string device_name, std::string property_name, auto_ptr<Data> data, std::string cycleName )
{
    try
    {
        AccessPoint & accessPoint = clientService_->getAccessPoint(device_name, property_name ,server_name_);
        auto_ptr<Data> filters = DataFactory::createData();
        std::auto_ptr<RequestContext> context = RequestContextFactory::create(cycleName, filters);
        accessPoint.set(data, context);
    }
    catch(const RdaException& ex)
    {
        printException(ex);
        throw ex;
    }
    catch(...)
    {
        std::cout << "Unknown Exception in executeSet" << std::endl;
        throw;
    }
}

void IntegrationTestRDAClient::executeFilteredSet( std::string device_name, std::string property_name, auto_ptr<Data> data, auto_ptr<Data> filter, std::string cycleName )
{
    try
    {
        AccessPoint & accessPoint = clientService_->getAccessPoint(device_name, property_name ,server_name_);
        std::auto_ptr<RequestContext> context = RequestContextFactory::create(cycleName, filter);
        accessPoint.set(data, context);
    }
    catch(const RdaException& ex)
    {
        printException(ex);
        throw ex;
    }
    catch(...)
    {
        std::cout << "Unknown Exception in executeFilteredSet" << std::endl;
        throw;
    }
}

std::auto_ptr<AcquiredData> IntegrationTestRDAClient::executeGet(std::string device_name, std::string property_name, std::string cycleName)
{
    try
    {
        AccessPoint & accessPoint = clientService_->getAccessPoint(device_name, property_name ,server_name_);
        auto_ptr<Data> filters = DataFactory::createData();
        std::auto_ptr<RequestContext> context = RequestContextFactory::create(cycleName, filters);
        return accessPoint.get( context );
    }
    catch(const RdaException& ex)
    {
        printException(ex);
        throw ex;
    }
    catch(...)
    {
        std::cout << "Unknown Exception in executeGet" << std::endl;
        throw;
    }
}

std::auto_ptr<AcquiredData> IntegrationTestRDAClient::executeFilteredGet(std::string device_name, std::string property_name, auto_ptr<Data> filter, std::string cycleName)
{
    try
    {
        AccessPoint & accessPoint = clientService_->getAccessPoint(device_name, property_name ,server_name_);
        std::auto_ptr<RequestContext> context = RequestContextFactory::create(cycleName, filter);
        return accessPoint.get( context );
    }
    catch(const RdaException& ex)
    {
        printException(ex);
        throw ex;
    }
    catch(...)
    {
        std::cout << "Unknown Exception in executeFilteredGet" << std::endl;
        throw;
    }
}

// Starts a subscription to the specified property
SubscriptionSharedPtr IntegrationTestRDAClient::subscribe(std::string device_name, std::string property_name, NotificationListenerSharedPtr replyHandler,  std::string cycleName)
{
    try
    {
        AccessPoint & accessPoint = clientService_->getAccessPoint(device_name, property_name,server_name_);
        auto_ptr<Data> filters = DataFactory::createData();
        std::auto_ptr<RequestContext> context = RequestContextFactory::create(cycleName, filters);
        SubscriptionSharedPtr sub = accessPoint.subscribe(context, replyHandler);
        return sub;
    }
    catch (const RdaException& ex)
    {
        printException(ex);
        throw ex;
    }

}

bool IntegrationTestRDAClient::unsubscribe(SubscriptionSharedPtr& sub)
{
    try
    {
        sub->unsubscribe();
        return true;
    }
    catch (const RdaException& ex)
    {
        printException(ex);
        throw ex;
    }
}

#endif // INTEGRATION_TEST_RDA_CLIENT_H_
"""

def template_test_rt_action_assert_notified_property_str(fesa_class_name, name_property="Fill Property Name"):
    return f"""
//                subscription = subscribe({fesa_class_name}_device_name,"{name_property}",replyHandler);

//                for(int i = 0 ; i < 5 ; i++)
//                {{
//                    std::cout << "Subscription run # " << i << std::endl;
//                    sleep(1);
//                    std::cout << "Data available: " << replyHandler->dataAvailable_ << std::endl;

//                    if( replyHandler->dataAvailable_ ){{
//// The cout below allows you to manually check the operation
//                        std::cout << "Data: " << replyHandler->receivedData_->getData().toString() << std::endl;
//                        std::cout << "Context: " << replyHandler->receivedData_->getContext().toString() << std::endl;
//// An appropriate assertion is suggested(example below)
//                        ASSERT_FALSE(replyHandler->receivedData_->getData().getBool("..."));
//                    }} else {{
//                        std::cout << "Subscription: no data received" << std::endl;
//                        std::cout << "Please make sure the property {name_property} is automatically notified." << std::endl;
//                    }}
//                }}
//                unsubscribe(subscription);
"""

def template_test_assert_rt_action_notified_property_str():
    return """
//                for(int i = 0 ; i < 5 ; i++)
//                {
//                    std::cout << "Step real-time action # " << i << std::endl;
//                    sleep(1);
//                    std::auto_ptr<AcquiredData> value = executeGet(device_name, "Fill Property Name");
//// The cout below allow you to manually check the operation
//                    std::cout << "Data: " << value->getData().toString() << std::endl;
//// An appropriate assertion is suggested(example below)
////                        ASSERT_TRUE(replyHandler->receivedData_->getData().getBool("..."));
//                }

"""

def template_test_rt_action_str(deploy_unit_name, fesa_class_name, rt_action_name,
                                                rt_action_notified_property_test):
    return f"""
    TEST_F(Test{deploy_unit_name}, {fesa_class_name}_RT_ACTION_{rt_action_name})
    {{
        SetUpDevice({fesa_class_name}_device_name);
//// Fill up as needed!
        ASSERT_NO_THROW(
//                TestListenerSharedPtr replyHandler(new IntegrationTestListener);
//// Below you will find only a test suggestion !!!
{rt_action_notified_property_test}
        );
    }}
"""

def template_makefile_str(deploy_unit_name, fesa_version):
    return f"""PROJECT = {deploy_unit_name}
PRODUCT = client
FESAFWK_VERSION = {fesa_version}

PARENT_MAKEFILE = /opt/cern/buildsystem/fesa/$(FESAFWK_VERSION)/Make.googletest

include $(PARENT_MAKEFILE)
# use "make test" to compile + link
# use "make run-test" to run the test
"""

def template_makefile_dep_str():
    return """FESAFWK_HOME = $(RELEASE_LOCATION_FWK)
DEPLOYED_CLASSES_HOME ?= /common/fesa/$(FESAFWK_VERSION)/fesa-equipment/$(FESAFWK_VERSION)/

RDA_HOME ?= $(MIDDLEWARE_LOCATION)/$(RDA_VERSION)
ZMQ_HOME ?= $(GSI_3RDPARTY_LOCATION)/zeromq/$(ZEROMQ_VERSION)
CTR_HOME ?= $(GSI_3RDPARTY_LOCATION)/gsi-ctr-lib/$(CTR_LIB_VERSION)
SAFT_HOME ?= /common/export/timing-rte/tg-fallout-v6.2.0/x86_64
CMX_HOME ?= /opt/cern/cmw/cmw-cmx/$(CMX_LIB_VERSION)

DEPENDENT_COMPILER_OPTIONS += -isystem$(FESAFWK_HOME)/include 
DEPENDENT_COMPILER_OPTIONS += -isystem$(SYSTEM_HOME)/include/libxml2
DEPENDENT_COMPILER_OPTIONS += -isystem$(ZMQ_HOME)/include
DEPENDENT_COMPILER_OPTIONS += -I$(CMW_UTIL_HOME)/include -I$(CMW_LOG_HOME)/include -I$(CMW_DATA_HOME)/include -I$(CMW_DIR_HOME)/include -I$(CMW_RDA_HOME)/include
DEPENDENT_COMPILER_OPTIONS += -isystem$(CTR_HOME)/include
DEPENDENT_COMPILER_OPTIONS += -isystem$(CMX_HOME)/include
DEPENDENT_COMPILER_OPTIONS += -I$(SAFT_HOME)/include/saftlib
DEPENDENT_COMPILER_OPTIONS += -I/usr/include/glib-2.0 -I/usr/lib64/glib-2.0/include -I/usr/include/sigc++-2.0
DEPENDENT_COMPILER_OPTIONS += -I/usr/lib64/sigc++-2.0/include


DEPENDENT_LINKER_OPTIONS += -L$(FESAFWK_HOME)/lib/$(CPU)
DEPENDENT_LINKER_OPTIONS += -L$(ZMQ_HOME)/lib
DEPENDENT_LINKER_OPTIONS += -L$(CMW_UTIL_HOME)/lib/$(CPU) -L$(CMW_LOG_HOME)/lib/$(CPU) -L$(CMW_DATA_HOME)/lib/$(CPU) -L$(CMW_DIR_HOME)/lib/$(CPU) -L$(CMW_RDA_HOME)/lib/$(CPU)
DEPENDENT_LINKER_OPTIONS += -L$(CTR_HOME)/lib/$(CPU)
DEPENDENT_LINKER_OPTIONS += -L$(BOOST_HOME)/lib/$(CPU)
DEPENDENT_LINKER_OPTIONS += -L$(CMX_HOME)/lib
DEPENDENT_LINKER_OPTIONS += -Wl,-Bstatic -lfesa-core-gsi -lctr-lib -lfesa-core \
-lcmw-rda3 -lcmw-data -lzmq -lcmw-log -lcmw-directory-client -lcmw-util -lboost_thread -lboost_system \
-lboost_atomic -lboost_chrono -lboost_filesystem -lboost_program_options \
-lcmw-cmx-cpp -lcmw-cmx \
-Wl,-Bdynamic -lm -lrt -ldl -lxml2
DEPENDENT_LINKER_OPTIONS += -Wl,-rpath=$(SAFT_HOME)/lib,-Bdynamic -L$(SAFT_HOME)/lib -lsaftlib -lsaftcommon
DEPENDENT_LINKER_OPTIONS += -lsigc-2.0
DEPENDENT_LINKER_OPTIONS += -L$(GELF_HOME)/lib/$(CPU) -lacosv-gelflib -lz
"""
