from src.string_templates import header_file_test_template_str

def generate_header_test_file(deploy_unit_name, fesa_class_test_file_path, fesa_class_name_list):

    device_definition_str = ""
    for fesa_class_name in fesa_class_name_list:
        device_definition_str += f"""std::string {fesa_class_name}_device_name = "";\n"""

    str_header_test_file_template = header_file_test_template_str(deploy_unit_name, device_definition_str)  # pylint: disable=C0301

    with open(f"{fesa_class_test_file_path}/src/test/Test{deploy_unit_name}.h", "w",
                encoding="utf-8") as header_test_file_template:
        header_test_file_template.write(str_header_test_file_template)
        print(f'\033[38;5;12m Generated Header FILE for {deploy_unit_name} Class! \033[0;0m')
