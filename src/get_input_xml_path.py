import os
import argparse
import sys

def get_input_xml_path():
    parser = argparse.ArgumentParser(
        description='Create template of file for test FESA class!',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument(
        'PATH_FESAClassDUXML',
        help='Path to the XML file of FESA Class DU.')

    parser.add_argument(
        '-f',
        '--force',
        action='store_true',
        help="Force the test template to be created from scratch.")

    args = parser.parse_args()

    path_fesa_class_du_xml = args.PATH_FESAClassDUXML[args.PATH_FESAClassDUXML.rfind('.')+1:]

    if path_fesa_class_du_xml != 'deploy':
        sys.exit('\033[2;31;231m The paths to the individual XML files entered are incorrect, correct them! \033[0;0m')  # pylint: disable=C0301


    return os.path.abspath(f'{args.PATH_FESAClassDUXML}/.'), \
            args.force
