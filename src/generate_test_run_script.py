import os

from src.string_templates import (script_template_to_configure_and_run_tests_str,
                                    template_makefile_dep_str, template_makefile_str,
                                    )

def generate_test_run_script(deploy_unit_name, fesa_class_test_file_path,
                                fesa_class_du_root, fesa_class_name_list):

    name_device, provide_device, attr_device = "", "", ""
    help_options, short_options, long_options, case_options = "", "", "", ""
    for index in range(len(fesa_class_name_list)):
        help_options += f"""#%    -d{index}, --{fesa_class_name_list[index]}_device_name   set name device for {fesa_class_name_list[index]}\n"""
        short_options += f"{index}:"
        long_options += f"""    [{fesa_class_name_list[index]}_device_name]=\"{index}\"\n"""
        case_options += f"""        \"{index}\") {fesa_class_name_list[index]}_device_name=${{OPTARG}} ;;\n"""
        name_device += f"""    {fesa_class_name_list[index]}_device_name=$(xmllint --xpath "string(//classes/{fesa_class_name_list[index]}/device-instance/@name)" "$(dirname "$run_instance_script")/"DeviceData_*.instance)\n"""  # pylint: disable=C0301
        provide_device += f"""if [ -z "${fesa_class_name_list[index]}_device_name" ]; then
    read -r -p "Please provide {fesa_class_name_list[index]} device name: " {fesa_class_name_list[index]}_device_name
fi\n"""
        attr_device += f" ${fesa_class_name_list[index]}_device_name"

    fesa_version = fesa_class_du_root.getElementsByTagName('fesa-version')[0].firstChild.data

    fesa_test_run_script_str = script_template_to_configure_and_run_tests_str(deploy_unit_name,
                                fesa_class_test_file_path, name_device, provide_device, attr_device, 
                                        help_options, short_options, long_options, case_options)

    with open(f"{fesa_class_test_file_path}/run_google_tests.sh", 'w',
                encoding="utf-8") as script_file:
        script_file.write(fesa_test_run_script_str)
        print(f'\033[38;5;12m Generated Run Tests Script for {deploy_unit_name} Class! \033[0;0m')

    with open(f"{fesa_class_test_file_path}/Makefile", 'w',
                encoding="utf-8") as script_file:
        script_file.write(template_makefile_str(deploy_unit_name, fesa_version))
        print(f'\033[38;5;12m Generated Makefile for {deploy_unit_name} Class! \033[0;0m')

    with open(f"{fesa_class_test_file_path}/Makefile.dep", 'w',
                encoding="utf-8") as script_file:
        script_file.write(template_makefile_dep_str())
        print(f'\033[38;5;12m Generated Makefile.dep for {deploy_unit_name} Class! \033[0;0m')
    os.chmod(f"{fesa_class_test_file_path}/run_google_tests.sh", 0o755)
