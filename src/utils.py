from src.string_templates import template_test_get_value_item_str, \
        template_test_set_and_get_value_item_str, template_assert_test_version_str, \
        template_assert_test_module_status_str, \
        template_assert_test_status_str, template_test_rt_action_str, \
        template_test_rt_action_assert_notified_property_str, \
        template_test_assert_rt_action_notified_property_str

def generate_test_for_set_or_get_value_item_of_property_data(deploy_unit_name, fesa_class_name,  # pylint: disable=R0913,R0914
                    fesa_class,fesa_class_interface_setting, fesa_class_interface_acquisition,
                                fesa_class_du_root, str_source_test_file_template, force_flag):
    fesa_class_property_data_test_str = ""

    for property_data_nodes in [fesa_class_interface_setting.childNodes,
                                fesa_class_interface_acquisition.childNodes]:
        for property_data_node in property_data_nodes:
            try:
                name = property_data_node.getAttribute('name')

                if len(property_data_node.getElementsByTagName('set-action')) == 1 and \
                        len(property_data_node.getElementsByTagName('get-action')) == 1:
                    if not force_flag and f'TEST_F(Test{deploy_unit_name}, {fesa_class_name}_Set_Get_{name})' \
                                                            in str_source_test_file_template: # pylint: disable=C0301
                        continue
                    fesa_class_property_data_test_str += template_test_set_and_get_value_item_str(
                                    deploy_unit_name, fesa_class_name, name,
                                    generate_append_item_to_data(property_data_node.childNodes))
                elif len(property_data_node.getElementsByTagName('get-action')) == 1:
                    if not force_flag and f'TEST_F(Test{deploy_unit_name}, {fesa_class_name}_Get_{name})' \
                                                    in str_source_test_file_template: # pylint: disable=C0301
                        continue
                    specific_assertions = ""
                    if property_data_node.tagName == "GSI-Version-Property":
                        fesa_version, class_version, deploy_unit_version = read_version_property_from_deploy_xml(fesa_class, fesa_class_du_root)  # pylint: disable=C0301
                        specific_assertions = template_assert_test_version_str(fesa_version,
                                                    class_version, deploy_unit_version)
                    elif property_data_node.tagName == "GSI-ModuleStatus-Property":
                        specific_assertions = template_assert_test_module_status_str()
                    elif property_data_node.tagName == "GSI-Status-Property":
                        specific_assertions = template_assert_test_status_str()
                    elif property_data_node.tagName == "GSI-Acquisition-Property":
                        specific_assertions = generate_assert_test_acquisition_str(
                                                    property_data_node)

                    fesa_class_property_data_test_str += template_test_get_value_item_str(
                                                            deploy_unit_name, fesa_class_name, name,
                                                            specific_assertions)

            except AttributeError:
                pass

    return fesa_class_property_data_test_str

def read_version_property_from_deploy_xml(fesa_class, fesa_class_du_root):
    fesa_version = fesa_class_du_root.getElementsByTagName('fesa-version')[0].firstChild.data
    # pylint: disable=C0301
    deploy_unit_version = f"{fesa_class_du_root.getElementsByTagName('deploy-unit-major-version')[0].firstChild.data}.\
{fesa_class_du_root.getElementsByTagName('deploy-unit-minor-version')[0].firstChild.data}.{fesa_class_du_root.getElementsByTagName('deploy-unit-tiny-version')[0].firstChild.data}"
    class_version = f"{fesa_class.getElementsByTagName('class-major-version')[0].firstChild.data}.\
{fesa_class.getElementsByTagName('class-minor-version')[0].firstChild.data}.{fesa_class.getElementsByTagName('class-tiny-version')[0].firstChild.data}"

    return fesa_version, class_version, deploy_unit_version

def generate_append_item_to_data(property_data_node):
    append_value = ""
    for property_data in property_data_node:
        try:
            check = True
            for tag in ['filter', 'update-flag', 'cycle-name', 'cycle-stamp', 'acq-stamp']:
                if tag in property_data.tagName:
                    check = False
                    break

            if 'item' in property_data.tagName and check:
                name_value_item = property_data.getAttribute('name')
                append_value += f"""//            data->append("{name_value_item}", ...);\n"""

        except AttributeError:
            pass

    return append_value

def generate_test_for_rt_action(deploy_unit_name, fesa_class_name, fesa_class_actions,
                            str_source_test_file_template, force_flag):
    fesa_class_rt_action_test_str = ""
    fesa_class_action_nodes = [fesa_class_action_node for fesa_class_action_node
                                    in fesa_class_actions.childNodes
                                    if "rt-action" in str(fesa_class_action_node)]

    for fesa_class_action_node in fesa_class_action_nodes:
        name = fesa_class_action_node.getAttribute('name')
        if not force_flag and f'TEST_F(Test{deploy_unit_name}, {fesa_class_name}_RT_ACTION_{name})' \
                                                in str_source_test_file_template: # pylint: disable=C0301
            continue

        rt_action_notified_property_test = ""
        notified_property_list = [notified_prop for notified_prop
                                        in fesa_class_action_node.childNodes
                                        if "notified-property" in str(notified_prop)]
        if len(notified_property_list) == 0:
            rt_action_notified_property_test += template_test_assert_rt_action_notified_property_str()  # pylint: disable=C0301
        else:
            rt_action_notified_property_test += """
//                SubscriptionSharedPtr subscription;
"""

            for notified_property in notified_property_list:
                notified_property_name = notified_property.getAttribute('property-name-ref')
                rt_action_notified_property_test += template_test_rt_action_assert_notified_property_str(fesa_class_name, notified_property_name)  # pylint: disable=C0301

        fesa_class_rt_action_test_str += template_test_rt_action_str(deploy_unit_name,
                                    fesa_class_name, name, rt_action_notified_property_test)

    return fesa_class_rt_action_test_str

def get_the_correct_getter(attr_type):
    getter = ""

    if attr_type == "bool":
        getter = "getBool"
    elif attr_type == "int8_t":
        getter = "getByte"
    elif attr_type == "int16_t":
        getter = "getShort"
    elif attr_type == "int32_t":
        getter = "getInt"
    elif attr_type == "int64_t":
        getter = "getLong"
    elif attr_type == "float":
        getter = "getFloat"
    elif attr_type == "double":
        getter = "getDouble"
    elif "char*" in attr_type:
        getter = "getString"

    return getter

def generate_assert_test_acquisition_str(gsi_acquisition_property):
    test_acquisition_str="\n// // This example only show how GSI-Acquisition-Property test can look like\n"  # pylint: disable=C0301

    acquisition_context_item_nodes = [node for node in gsi_acquisition_property.childNodes
                                                if "acquisition-context-item" in str(node)]

    if len(acquisition_context_item_nodes) != 1:
        return test_acquisition_str

    for node in acquisition_context_item_nodes[0].childNodes:
        try:
            name = node.getAttribute('name')
            attr_type = node.getElementsByTagName('scalar')[0].getAttribute('type')
            getter = get_the_correct_getter(attr_type)

            test_acquisition_str += f'''
//            {attr_type} {name}field = value->getData().{getter}("{name}");
//            ASSERT_EQ({name}field, ...);'''

        except AttributeError:
            pass
        except IndexError:
            pass

    return test_acquisition_str
