from xml.dom import minidom

from src.string_templates import (begining_of_source_test_file_str,
                                    __ending_of_source_test_file_str__)
from src.utils import (generate_test_for_set_or_get_value_item_of_property_data,
                            generate_test_for_rt_action)

def generate_source_test_file(deploy_unit_name, fesa_class_list, fesa_class_du_root,   # pylint: disable=R0914,R0913
                            fesa_class_test_file_path, force_flag, fesa_class_name_list):

    device_declaration_str = ""
    for index in range(len(fesa_class_name_list)):
        device_declaration_str += f"    Test{deploy_unit_name}::{fesa_class_name_list[index]}_device_name = std::string(argv[{index+2}]);\n"  # pylint: disable=C0301
    str_source_test_file_template = begining_of_source_test_file_str(deploy_unit_name, device_declaration_str)  # pylint: disable=C0301
    if not force_flag:
        with open(f"{fesa_class_test_file_path}/src/test/Test{deploy_unit_name}.cpp", "r",
                encoding="utf-8") as source_test_file_template:
            tmp_str = source_test_file_template.read()
            str_source_test_file_template += tmp_str[tmp_str.find('// Make your changes below') + 27
                                                        :tmp_str.rfind('// End of making changes')]

    for index in range(len(fesa_class_list)):
        fesa_class = fesa_class_list[index]
        fesa_class_name = fesa_class_name_list[index]
        fesa_class_path = fesa_class.getElementsByTagName('path')[0].firstChild.data
        fesa_class_xml_path = f'{fesa_class_path}/src/{fesa_class_name}.design'
        fesa_class_root = minidom.parse(fesa_class_xml_path)

        fesa_class_interface = fesa_class_root.getElementsByTagName('device-interface')[0]
        fesa_class_actions = fesa_class_root.getElementsByTagName('actions')[0]

        fesa_class_interface_setting = fesa_class_interface.getElementsByTagName('setting')[0]
        fesa_class_interface_acquisition = fesa_class_interface.getElementsByTagName('acquisition')[0]# pylint: disable=C0301


        str_source_test_file_template += generate_test_for_set_or_get_value_item_of_property_data(deploy_unit_name, fesa_class_name,fesa_class,  # pylint: disable=C0301
                                            fesa_class_interface_setting, fesa_class_interface_acquisition,  # pylint: disable=C0301
                                            fesa_class_du_root, str_source_test_file_template,  # pylint: disable=C0301
                                            force_flag)
        str_source_test_file_template += generate_test_for_rt_action(deploy_unit_name,
                                                fesa_class_name, fesa_class_actions,
                                                str_source_test_file_template, force_flag)

    str_source_test_file_template += __ending_of_source_test_file_str__

    with open(f"{fesa_class_test_file_path}/src/test/Test{deploy_unit_name}.cpp", "w",
                encoding="utf-8") as source_test_file_template:
        source_test_file_template.write(str_source_test_file_template)
        print(f'\033[38;5;12m Generated Source FILE for {deploy_unit_name} Class! \033[0;0m')
