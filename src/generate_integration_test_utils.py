from src.string_templates import (intergration_test_listener_template_str,
                                    intergration_test_rda_client_template_str)

def generate_integration_test_listener(deploy_unit_name, fesa_class_test_file_path):

    with open(f"{fesa_class_test_file_path}/src/test/include/IntegrationTestListener.h", "w",
                encoding="utf-8") as integration_test_listener_template:
        integration_test_listener_template.write(intergration_test_listener_template_str())
        print(f'\033[38;5;12m Generated IntegrationTestListener for {deploy_unit_name} Class! \033[0;0m')  # pylint: disable=C0301

    with open(f"{fesa_class_test_file_path}/src/test/include/IntegrationTestRDAClient.h", "w",
                encoding="utf-8") as integration_test_rda_client_template:
        integration_test_rda_client_template.write(intergration_test_rda_client_template_str())
        print(f'\033[38;5;12m Generated IntegrationTestRDAClient for {deploy_unit_name} Class! \033[0;0m')  # pylint: disable=C0301
