# fesa-test-generator

The FESA test generator allows you to automatically generate a class test framework.

## Usage FESA Test Generator


```
usage: fesa-test-generator [-h] [-f] PATH_FESAClassDUXML

Create template of file for test FESA class!

positional arguments:
  PATH_FESAClassDUXML  Path to the XML file of FESA Class DU.

optional arguments:
  -h, --help           show this help message and exit
  -f, --force          Force the test template to be created from scratch.
                       (default: False)
```
Requirments:
* python version 3

Sample use:
* From the project source

        python3 fesa_test_generator.py FESAClassDUDirectory/src/FESAClass.deploy -f
        ./fesa_test_generator.py FESAClassDUDirectory/src/FESAClass.deploy -f

* Globally, being on the asl74* or asl75* server

        fesa-test-generator FESAClassDUDirectory/src/FESAClass.deploy -f

The test framework will be generated for all FESA Class included in Deploy FILE.
The startup scripts are located in the deploy unit directory (/src/Test{deploy_unit_name}).

## Usage Generated Google Tests

To perform the tests, use the prepared script generated in the class path fesa deploy unit/src/Test{deploy_unit_name}.
```
./run_google_tests.sh -h
 SYNOPSIS
    run_google_tests.sh [-d*] [-s] [-i] [-r]

 DESCRIPTION
    This script is used to compile and run google FESA tests

 OPTIONS
    -d*, --FESAClass_device_name   set name device for FESAClass
    -s, --server_name             set the server name value
    -i, --instance_script_run     set the path to instance script runner
    -r, --ready_flag              set the flag on, server is running
    -h, --help, --man             print this help

 SOURCE
    https://git.gsi.de/t.madej/fesa-test-generator
```
`*` - depending on the number of fesa class belonging to the deploy unit, subsequent classes symbolize d0, d1, ... <br/>
All options are optional.
Specifying a path to instance script run will select the first device belonging to this class, run server and run tests for them.
Otherwise, start the fesa server manually, the script asks if it is running unless ready_flag is set then we assume that it is running.
