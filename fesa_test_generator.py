#!/usr/bin/env python3

import os
from xml.dom import minidom

from src.get_input_xml_path import get_input_xml_path
from src.generate_header_test_file import generate_header_test_file
from src.generate_source_test_file import generate_source_test_file
from src.generate_test_run_script import generate_test_run_script
from src.generate_integration_test_utils import generate_integration_test_listener

def generate_fesa_test():

    fesa_class_du_xml_path, force_flag = get_input_xml_path()
    fesa_class_du_root = minidom.parse(fesa_class_du_xml_path)

    deploy_unit_name = fesa_class_du_root.getElementsByTagName('deploy-unit-name')[0].firstChild.data  # pylint: disable=C0301
    fesa_class_list = fesa_class_du_root.getElementsByTagName('class')
    fesa_class_du_path=fesa_class_du_xml_path[:fesa_class_du_xml_path.rfind('src')-1]
    fesa_class_name_list = [fesa_class.getElementsByTagName('class-name')[0].firstChild.data
                                                            for fesa_class in fesa_class_list]

    fesa_class_test_file_path = f'{fesa_class_du_path}/src/Test{deploy_unit_name}'
    # Check whether the specified path exists or not
    is_exist = os.path.exists(f'{fesa_class_test_file_path}/src/test/include')
    if not is_exist:
        # Create a new directory because it does not exist
        os.makedirs(f'{fesa_class_test_file_path}/src/test/include')
        print(f"The test directory is created!\nas: {fesa_class_test_file_path}")
        force_flag = True
    elif not os.path.exists(f'{fesa_class_test_file_path}/src/test/Test{deploy_unit_name}.cpp'):
        force_flag = True

    print(f'\033[38;5;12m Start Generating Tests for FESA {deploy_unit_name} Class \033[0;0m')
    generate_test_run_script(deploy_unit_name, fesa_class_test_file_path,
                                fesa_class_du_root, fesa_class_name_list)
    generate_header_test_file(deploy_unit_name, fesa_class_test_file_path, fesa_class_name_list)
    generate_integration_test_listener(deploy_unit_name, fesa_class_test_file_path)

    generate_source_test_file(deploy_unit_name, fesa_class_list, fesa_class_du_root,
                            fesa_class_test_file_path, force_flag, fesa_class_name_list)

    print(f'\033[2;32;231m End Generating Tests for FESA {deploy_unit_name} Class \033[0;0m')

    print('\033[2;32;231m DONE! \033[0;0m')


if __name__ == '__main__':
    generate_fesa_test()
